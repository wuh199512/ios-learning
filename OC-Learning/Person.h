//
//  Person.h
//  OC-Learning
//
//  Created by bytedance on 2021/7/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
@property(nonatomic, readwrite, copy) NSString * name;
@property(nonatomic, readwrite, copy) NSString * sex;
@property(nonatomic, readwrite) NSDate * birthday;
@property(nonatomic, readwrite, copy) NSString * hoppy;
@end
NS_ASSUME_NONNULL_END
