//
//  Person.m
//  OC-Learning
//
//  Created by bytedance on 2021/7/9.
//

#import "Person.h"

@implementation Person
@synthesize name;
@synthesize sex;
@synthesize birthday;
@synthesize hoppy;
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"吴煌彬";
        self.sex = @"男";
        self.hoppy = @"吃饭睡觉打豆豆";        
        NSString * dateString = @"1995-12-29";
        NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"yyyy-MM-dd"];
        [fmt setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
        self.birthday = [fmt dateFromString:dateString];
    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"我的名字：%@, 性别：%@, 生日：%@, 我喜欢%@", name, sex, birthday, hoppy];
}
@end
